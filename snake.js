let fps, now, then, delta, interval, canvas, ctx, keysDown, grid, snake, apple;

/** 
 * Basic vector which takes parameters x and y
 */
function Vector2(x, y) {
  this.x = x;
  this.y = y;
  this.add = (other) => new Vector2(this.x + other.x, this.y + other.y);
  this.multiply = (n) => new Vector2(this.x * n, this.y * n);
  this.equals = (other) => this.x === other.x && this.y === other.y;
  this.toString = () => "<" + this.x + ", " + this.y + ">";
}

function frameRateSettings(framesPerSecond = 12) {
  then = Date.now();
  interval = 1000 / framesPerSecond;
}

function canvasSettings(grid) {
  canvas = document.createElement("canvas");
  ctx = canvas.getContext("2d");

  canvas.setup = function (grid) {
    this.width = grid.scale.x * grid.pixelSize;
    this.height = grid.scale.y * grid.pixelSize;
    this.style.backgroundColor = "black";
  }

  canvas.setup(grid);
  document.body.appendChild(canvas);
}

/**
 * Adds event listeners for keyboard input
 */
function eventListenerSettings() {
  keysDown = {};

  addEventListener("keydown", function (e) {
    keysDown[e.keyCode] = true;
  }, false);

  addEventListener("keyup", function (e) {
    delete keysDown[e.keyCode];
  }, false);
}

/**
 * Creates the game objects: grid, snake, and apple
 */
function sceneSettings() {
  grid = {
    "scale": new Vector2(20, 20),
    "pixelSize": 20,
    "color": "green",
    "draw": function (ctx) {
      ctx.fillStyle = this.color;
      ctx.fillRect(0, 0, this.scale.x * this.pixelSize, this.scale.y * this.pixelSize);
    },
    "drawPoint": function drawPoint(point, color, ctx) {
      ctx.fillStyle = color;
      ctx.fillRect(point.x * this.pixelSize, point.y * this.pixelSize, this.pixelSize, this.pixelSize);
    },
    "randomPosition": function () {
      let randRound = (n) => Math.round(Math.random() * (n - 1));
      return new Vector2(randRound(this.scale.x), randRound(this.scale.x));
    },
  };

  snake = {
    "color": "blue",
    "velocity": new Vector2(0, 0),
    "body": [new Vector2(3, 9), new Vector2(2, 9), new Vector2(1, 9)],
    "reset": function () {
      this.body = [new Vector2(3, 9), new Vector2(2, 9), new Vector2(1, 9)]
    },
    "move": function () {
      if (!this.velocity.equals(new Vector2(0, 0)) && !this.body[0].add(this.velocity).equals(this.body[1])) {
        this.body.unshift(this.body[0].add(this.velocity));
        this.body.pop();
      }
    },
    "draw": function (ctx, grid) {
      this.body.forEach(point => {
        grid.drawPoint(point, this.color, ctx);
      });
    },
    "grow": function () {
      this.body.unshift(this.body[0].add(this.velocity));
    },
    "outOfBounds": function (grid) {
      return this.body[0].x < 0 || this.body[0].x > grid.scale.x - 1 || this.body[0].y < 0 || this.body[0].y > grid.scale.y - 1
    },
    "selfCollide": function () {
      for (let i = 1; i < this.body.length; i++) {
        if (this.body[0].equals(this.body[i])) {
          return true;
        }
      }
      return false;
    },
    "freeze": function () {
      this.velocity = new Vector2(0, 0);
    },
    "contains": function (apple) {
      for (let i = 0; i < snake.body.length; i++) {
        if (apple.position.equals(snake.body[i])) {
          return true;
        }
      }
      return false;
    },
  };

  apple = {
    "color": "red",
    "position": new Vector2(15, 9),
    "draw": function (ctx, grid) {
      grid.drawPoint(this.position, this.color, ctx);
    },
    "reset": function (grid, snake) {
      do {
        this.position = grid.randomPosition();
      } while(snake.contains(this));
    },
  };
}

/**
 * Update the game each frame
 */
function update() {  
  keyPressed();

  // Update snake movement
  if (snake.outOfBounds(grid) || snake.selfCollide()) {
    snake.reset();
  } else {
    snake.move();
  }

  // Snake eats apple
  if (snake.body[0].equals(apple.position)) {
    snake.grow();
    apple.reset(grid, snake);
  }

  render();
}

/** 
 * Pause the game by freezing the snake
 */
function pause() {
  snake.freeze();
}

/**
 * Execute code when a key is pressed
 * 
 * Mainly used for controlling the snake's movement
 */
function keyPressed() {
  // Assign key codes
  let up = (38 in keysDown || 87 in keysDown);
  let down = (40 in keysDown || 83 in keysDown);
  let left = (37 in keysDown || 65 in keysDown);
  let right = (39 in keysDown || 68 in keysDown);
  let spacebar = 32 in keysDown;

  // Control the snake's direction
  if (up && snake.velocity.y !== 1) {
    snake.velocity = new Vector2(0, -1);
  } else if (down && snake.velocity.y !== -1) {
    snake.velocity = new Vector2(0, 1);
  } else if (left && snake.velocity.x !== 1) {
    snake.velocity = new Vector2(-1, 0);
  } else if (right && snake.velocity.x !== -1) {
    snake.velocity = new Vector2(1, 0);
  }

  if (spacebar) {
    pause();
  }
}

/**
 * Draw all objects in the game
 */
function render() {
  grid.draw(ctx);
  snake.draw(ctx, grid);
  apple.draw(ctx, grid);
}

/**
 * Configuration and settings before the game starts
 */
function settings() {
  sceneSettings();
  frameRateSettings(framesPerSecond = 12);
  canvasSettings(grid);
  eventListenerSettings();
}

/**
 * Continuously loop the game
 */
function gameLoop() {
  window.requestAnimationFrame(gameLoop);
  now = Date.now();
  delta = now - then;

  if (delta > interval) {
    then = now - (delta % interval);
    update();
  }
}

function main() {
  settings();
  window.requestAnimationFrame(gameLoop);
}